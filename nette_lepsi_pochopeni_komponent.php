<?php

class InputText extends Container
{


    /**
     * 
     * @return string
     */
    public function render()
    {
        $output = "";

        $output .= "<input type=\"text\" name=" . $this->getName() . " />";

        return $output;
    }

}

class InputFile extends Container
{


    /**
     * Řikejme tomu 'událost', která nastane po připojení této komponenty k formuláři.
     * @param IContainer $obj
     */
    protected function attached($obj)
    {
        // $obj je komponenta, která si veme objekt pod svá křídla tj. formulář/rodič
        parent::attached($obj);

        $obj->setEnctype('multipart/form-data');
    }


    /**
     * 
     * @return string
     */
    public function render()
    {
        $output = "";

        $output .= "<input type=\"file\" name=" . $this->getName() . " />";

        return $output;
    }

}

class InputSubmit extends Container
{


    /**
     * 
     * @return string
     */
    public function render()
    {
        $output = "";

        $output .= "<input type=\"submit\" name=" . $this->getName() . " />";

        return $output;
    }

}

class Form extends Container
{

    /** @var string */
    private $enctype = 'text/plain';

    /** @var string */
    private $method = 'post';

    /** @var string */
    private $action = '/';


    /**
     * set enctype
     * @param string $enctype
     * @return \Form
     */
    public function setEnctype($enctype)
    {
        $this->enctype = $enctype;

        return $this;
    }


    /**
     * get form enctype value
     * @return string
     */
    public function getEnctype()
    {
        return $this->enctype;
    }


    /**
     * Set method
     * @param string $method
     * @return \Form
     */
    public function setMethod($method)
    {
        if ($method != 'post' || $method != 'get') {
            throw new FormException('Method value must be: \'post\' or \'get\'');
        }

        $this->method = $method;

        return $this;
    }


    /**
     * get form method value
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }


    /**
     * 
     * @param type $action
     * @return \Form
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }


    /**
     * get form action value
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }


    /**
     * 
     * @param string $name
     */
    public function addText($name)
    {
        $this->addComponent(new InputText, $name);
    }


    /**
     * 
     * @param string $name
     */
    public function addFile($name)
    {
        $this->addComponent(new InputFile, $name);
    }


    /**
     * 
     * @param string $name
     */
    public function addSubmit($name)
    {
        $this->addComponent(new InputSubmit, $name);
    }


    /**
     * render full form (with inputs)
     * @return string
     */
    public function render()
    {
        $output = '';

        $output .= "<form name=\"" . $this->getName() .
                "\" action=\"" . $this->getAction() .
                "\" method=\"" . $this->getMethod() .
                "\" enctype=\"" . $this->getEnctype() .
                "\">";

        foreach ($this->getComponents() as $component) {
            $output .= $component->render();
            $output .= '
';
        }

        $output .= "</form>";

        return $output;
    }

    /** TO IMPLEMENT... */

}

class FormException
{
    
}

$form = new Form(NULL, 'formular');

$form->addText('email');

$form->addFile('file'); // v tento okamžik potřebujeme změnit property enctype na jinou hodnotu
$form->addSubmit('send');

echo $form->render();
