<?php

/**
 * @author: Martin Liprt
 * @email: tuxcoffe@gmail.com
 * https://cs.wikipedia.org/wiki/Morrisonova_%C4%8D%C3%ADseln%C3%A1_%C5%99ada
 */
class MorrisonPrincipe
{

    /** @var int */
    private $n;

    /** @var int */
    private $k;

    /** @var array */
    private $list = [];


    /**
     * constructor
     * @param int $n
     * @param int $k
     */
    public function __construct($n, $k)
    {
        $this->n = $n;
        $this->k = $k;
    }


    /**
     * transform
     * @return array
     */
    public function transform()
    {
        $fromNumber = 1;

        for ($i = 0; $i < $this->n; $i++) {
            $fromNumber = $this->transformNumber($fromNumber);
            $this->list[] = $fromNumber;
        }

        return $this->list;
    }


    /**
     * transform single number
     * @param int $number
     * @return string
     */
    public function transformNumber($number)
    {
        $number = (string) $number;
        $transformedNumber = '';

        for ($j = 0; $j < strlen($number); $j++) {
            $currentNumber = $number[$j];
            $nextNumber = isset($number[$j + 1]) ? $number[$j + 1] : false;
            $numberCounts++;

            if ($currentNumber != $nextNumber) {
                $currentNumberCount = $numberCounts . $currentNumber;
                $transformedNumber .= $currentNumberCount;
                $numberCounts = 0;
            }
        }

        return $transformedNumber;
    }

}

$morrisonPrincipe = new MorrisonPrincipe(8, 10);
print_r($morrisonPrincipe->transform());